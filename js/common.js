'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

$(function() {

  // placeholder
  //-----------------------------------------------------------------------------
  $('input[placeholder], textarea[placeholder]').placeholder();

  $('.data-birth').mask("99.99.9999");
  $('.phone').mask("+7(999)999-99-99");

  $('.select-style').select2();

  function nextStep(step) {
    $('.' + step + '').show();
  }


  jQuery.validator.addMethod("cyrillic", function(value, element) {
    return this.optional(element) || /^[\-А-ЯЁа-яё]{1,30}$/.test(value);
  }, 'Please enter a valid email address.');


  $('.step-1 form').validate({
    onkeyup: true,
    rules: {
      name: {
        required: true,
        cyrillic: true
      },
      lastName: {
        required: true,
        cyrillic: true
      },
      gander: {
        required: true
      },
    },
    messages: {
      name: {
        required: 'Пожалуйста, укажи свое имя. Допускаются только буквы кириллицы и дефис',
        cyrillic: 'Пожалуйста, укажи свое имя. Допускаются только буквы кириллицы и дефис'
      },
      lastName: {
        required: 'Пожалуйста, укажи свою фамилию. Допускаются только буквы кириллицы и дефис',
        cyrillic: 'Пожалуйста, укажи свое имя. Допускаются только буквы кириллицы и дефис'
      },
      gander: {
        required: 'Пожалуйста, укажи пол'
      },
    }
  });


  $('.step-2 form').validate({
    rules: {
      age: {
        required: true,
        conditional: function() {
          var $field = $('#BirthDate');
          var $error = $field.parent().find('label.error');
          var today = new Date();
          var birthday = $field.val().split('.');

          if (!isBirthdayCorrect(birthday)) {
            $('.error-correct').show();
            $('.error-18').hide();
            return;
          } else {
            $('.error-correct').hide();
          }

          function isBirthdayCorrect(birthday) {
            if (+birthday[0] < 1 || +birthday[0] > 31) return;
            if (+birthday[1] < 1 || +birthday[1] > 12) return;
            if (+birthday[2] < (new Date()).getFullYear() - 100) return;
            return true;
          }

          birthday = new Date(+birthday[2], +birthday[1] - 1, +birthday[0]);
          var years = today.getFullYear() - birthday.getFullYear();
          if (years < 18) {
            $('.error-18').show();
          } else {
            $('.error-18').hide();
          }
        }
      },
      town: {
        required: true
      },
    },
    messages: {
      age: {
        required: ''
      },
      town: {
        required: 'Пожалуйста, укажи город'
      },
    }
  });

  $('.step-3 form').validate({
    rules: {
      phone: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
    },
    messages: {
      phone: {
        required: 'Пожалуйста, укажи номер мобильного телефона'
      },
      email: {
        required: 'Введите Email',
        email: 'Введите валидный Email'
      },
    }
  });
  //
  $('.step-4 form').validate({
    rules: {
      brand1: {
        selectcheck: true
      },
      brand2: {
        selectcheck: true
      },
      rules: {
        required: true
      },
    },
    messages: {
      brand1: {
        required: 'Укажите бренд',
        selectcheck: 'Укажите бренд'
      },
      brand2: {
        required: 'Укажите бренд',
        selectcheck: 'Укажите бренд'
      },
      rules: {
        required: 'Вы должны согласится'
      },
    }
  });

  //
  $('.step-5 form').validate({
    rules: {
      passport: {
        required: true
      },
      passportNum: {
        required: true
      },
    },
    messages: {
      passport: {
        required: 'Введите серию паспорта'
      },
      passportNum: {
        required: 'Введите номер паспорта'
      },
    }
  });

  $('.step-7 form').validate({
    rules: {
      unicNum: {
        required: true
      },
    },
    messages: {
      unicNum: {
        required: 'Введите код'
      },
    }
  });

  jQuery.validator.addMethod('selectcheck', function(value) {
    if (value !== 0) {
      return (value != '0');
    }

  }, "year required");


  $(".steps form").submit(function(e) {
    var isvalidate = $(this).valid();
    if (isvalidate) {
      e.preventDefault();
      var next = $(this).find('.btn').attr('id')
      $(this).closest('.steps').hide()
      nextStep(next);
    }
  });

  function fetchLocationCityId(city) {
    $.ajax({
      url: $.kladr.url,
      method: 'GET',
      dataType: 'jsonp',
      data: {
        token: $.kladr.token,
        contentType: 'city',
        limit: 1,
        query: city
      },
      cache: false,
      crossDomain: true
    }).done(function(data) {
      if (!data.result.length) return;
      $('#CitySearch').val(data.result[0].name);
    });
  }

  var regForm = $('#RegForm');

  // $('#MobilePhoneNumber,#Email').blur(function() {
  //   var tempVal = $(this).val();
  //   dataCheck(tempVal);
  // })
  //
  // function dataCheck(tempVal) {
  //   console.log(tempVal);
  //   $.ajax({
  //     url: regForm.attr('data-check'),
  //     type: regForm.attr('method'),
  //     dataType: 'json',
  //     success: function(data, textStatus, jqXHR) {}
  //   });
  // }

  $('#Email, #MobilePhoneNumber').change(function() {
        var field = $(this);
        $.ajax({
            url: field.attr('data-action'),
            type: 'POST',
            data: field.val(),
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                if (data.error == true) {
                  field.parent().addClass('has-error')
                } else{
                  field.parent().removeClass('has-error')
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    });


  var brandUrl = regForm.find('#Cigarette').attr('data-action');
  var brandExtUrl = regForm.find('#CigaretteExt').attr('data-action');

  var brandUrl = regForm.find('#Cigarette').attr('data-action');
  var brandExtUrl = regForm.find('#CigaretteExt').attr('data-action');


  function brandSelectConfigAjax(url, data) {
    return {
      url: url,
      method: 'GET',
      dataType: 'json',
      data: data,
      cache: false,
      processResults: function(data) {
        if (data.error) return;
        return {
          results: brandSelectProcessResults(data)
        };
      }
    }
  }


  function brandSelectProcessResults(data) {
    var arr = [];
    var brandID = '';

    for (var i = 0, item, text, len = data.result.length; i < len; i++) {
      item = data.result[i];

      if (brandID !== item.family_id && !data.no_diff) {
        arr.push({
          text: item.family_name
        });
        brandID = item.family_id;
      }

      text = item.family_name;
      if (!data.no_diff) text += ' ' + item.diff_name;

      arr.push({
        family_id: item.family_id,
        family_name: item.family_name,
        //id: item.diff_id,
        id: item.diff_id || item.family_id,
        diff_id: item.diff_id,
        diff_name: item.diff_name,
        text: text,
        pmi: item.pmi
      });
    }

    return arr;
  }

  function brandSelectConfig(key, val) {
    var config = {
      language: 'ru',
      minimumInputLength: 1,
      templateResult: function(state) {
        return $(state.id ? '<span />' : '<strong />').text(state.text);
      }
    };

    config[key] = val;
    return config;
  }

  //function brandSelectShow(brandSelect, data) {
  function brandSelectShow(brandSelect) {
    brandSelect
      .on('select2:selecting', function() {
        $(this).val(null);
      })
      .on('select2:select', function(e) {
        //$(this).prevAll().filter('#CigaretteBrandExt').val(data.family_id);
        //$(this).prevAll().filter('#CigaretteTypeExt').val(data.id);
        //$(this).prevAll().filter('#CigaretteTypeExt').val(data.diff_id);
        $(this).prevAll().filter('#CigaretteBrandExt').val(e.params.data.family_id);
        $(this).prevAll().filter('#CigaretteTypeExt').val(e.params.data.diff_id);
        $(this).next().find('.select2-search__field').blur();
      });

    regForm.find('.form-brands').addClass('selected');
  }

  var primeBrandSelect = regForm.find('#Cigarette');
  var extBrandSelect = regForm.find('#CigaretteExt');

  primeBrandSelect
    .select2(brandSelectConfig('ajax', brandSelectConfigAjax(brandUrl, function(params) {

      return {
        brand: params.term
      };
    })))
  .on('select2:selecting', function() {
      $(this).val(null);
    })
    .on('select2:select', function(e) {
      $(this).prevAll().filter('#CigaretteBrand').val(e.params.data.family_id);
      //$(this).prevAll().filter('#CigaretteType').val(e.params.data.id);
      $(this).prevAll().filter('#CigaretteType').val(e.params.data.diff_id);
      $(this).next().find('.select2-search__field').blur();
      var len = e.params.data.text.length;
      var pmi = extBrandSelect.attr('data-pmi');
      var extBrandSelectObj;

      var request = {
        location_id: $('[name="RegistrationForm[locationId]"]').val(),
        brand_id: $('#CigaretteBrand').val(),
        diff_id: $('#CigaretteType').val()
      };

      if (!len) regForm.find('.form-brands').removeClass('selected');

      if (len && pmi !== e.params.data.pmi) {
        if (extBrandSelect.is('[class*="select2"]')) {
          extBrandSelect.val(null);
          extBrandSelect.select2('destroy');
        }

        if (e.params.data.pmi) {
          // init select with ajax
          extBrandSelectObj = extBrandSelect
            .select2(brandSelectConfig('ajax', brandSelectConfigAjax(brandExtUrl, function(params) {
              request.brand = params.term;
              return request;
            })));

          //brandSelectShow(extBrandSelectObj, e.params.data);
          brandSelectShow(extBrandSelectObj);
        } else {
          // preload brands and init select with data
          $.ajax({
            url: brandExtUrl,
            method: 'GET',
            dataType: 'json',
            data: request,
            cache: false
          }).done(function(data) {
            if (data.error) {
              messages.error(data.message, 5000);
              return;
            }

            extBrandSelectObj = extBrandSelect
              .select2({
                'data': brandSelectProcessResults(data)
              });

            //brandSelectShow(extBrandSelectObj, e.params.data);
            brandSelectShow(extBrandSelectObj);
          }).fail(function(jqXHR, textStatus, errorThrown) {
            messages.error('Ошибка сервера', 5000);
          });
        }
      }

      regForm.find('#BrandExtTitle span').html(e.params.data.family_name);
      extBrandSelect.attr('data-pmi', e.params.data.pmi);
    });




  $("#CitySearch")
    .keydown(function(e) {
      if (e.keyCode === 13) return false;
    })
    .kladr({
      token: $.kladr.token,
      type: $.kladr.type.city,
      change: function(obj) {}
    });
});
